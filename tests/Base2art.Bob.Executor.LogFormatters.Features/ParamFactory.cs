﻿namespace Base2art.Bob.Executor.LogFormatters
{
    using System;
    using Base2art.Bob.Executor.Procedures;
    
    public static class ParamFactory
    {

        public static TargetExecutionParameters TargetExectionParams<T>(this T obj)
        {
            return new TargetExecutionParameters(
                Guid.Empty, 
                "c:\\Temp\\wd", 
                "c:\\Temp\\artifacts", 
                TargetExecutionStatus.Failure,
                new LifecyclePosition());
        }
    }
}
