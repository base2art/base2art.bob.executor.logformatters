﻿namespace Base2art.Bob.Executor.LogFormatters
{
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class SerializerFeature
    {
        [Test]
        public void ShouldSerialize()
        {
            var result = Serializer.Serialize(new {ABC = 123});
            result.Should().Be("{\r\n  \"ABC\": 123\r\n}");
        }
        
        [Test]
        public void ShouldDeserialize()
        {
            var result = Serializer.Deserialize<FakeObj>("{\r\n  \"ABC\": 123\r\n}");
            result.ABC.Should().Be(123);
        }
        
        private class FakeObj
        {
            public int ABC { get; set; }
        }
    }
}
