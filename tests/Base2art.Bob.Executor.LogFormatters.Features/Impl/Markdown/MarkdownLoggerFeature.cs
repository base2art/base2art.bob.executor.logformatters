﻿
namespace Base2art.Bob.Executor.LogFormatters.Markdown
{
    using System;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class MarkdownLoggerFeature
    {
        [SetUp]
        public void BeforeEach()
        {
            var artifacts = new DirectoryInfo(this.TargetExectionParams().DefaultArtifactDirectory);
            artifacts.Create();
            foreach (var file in artifacts.GetFiles())
            {
                file.Delete();
            }
        }
        
        [Test]
        public void ShouldCreate()
        {
            var factory = new MarkdownLoggerFactory();
            factory.CreateLoggerFor(this.TargetExectionParams(), new DependencyIdentifier(), new DependencyIdentifier())
                .Should().BeOfType<MarkdownLogger>()
                .And.Subject.As<MarkdownLogger>().Id.Should().NotBe(Guid.Empty);
            
            factory.CreateLoggerFor(this.TargetExectionParams(), Guid.Empty, new DependencyIdentifier(), new DependencyIdentifier())
                .Should().BeOfType<MarkdownLogger>()
                .And.Subject.As<MarkdownLogger>().Id.Should().NotBe(Guid.Empty);
        }
        
        [Test]
        public void ShouldVerifyProperties()
        {
            new MarkdownMetadataTree().Assert().NeverNull(x => x.Children);
        }
        
        [Test]
        public void ShouldWriteToDisk()
        {
            using (var twOut = new StringWriter())
            {
                using (var twDebug = new StringWriter())
                {
                    using (var twError = new StringWriter())
                    {
                        var factory = new MarkdownLoggerFactory();
                        var echo = Echo(factory);
                        var echoChildStatus = Status(factory, echo.Id);
                        var status = Status(factory, null);
                        
                        var statusChildStatus = Status(factory, status.Id);
                        
                    }
                }
            }
        }
        
        [Test]
        public void ShouldWriteToDisk_Recursion()
        {
            using (var twOut = new StringWriter())
            {
                using (var twDebug = new StringWriter())
                {
                    using (var twError = new StringWriter())
                    {
                        var factory = new MarkdownLoggerFactory();
                        var echo = Echo(factory);
                        var echoChildStatus = Status(factory, echo.Id);
                        var echoChildStatusChildStatus = Status(factory, echoChildStatus.Id);
                    }
                }
            }
        }

        private IProcedureLogger Echo(MarkdownLoggerFactory factory)
        {
            var dependencyIdentifier = new DependencyIdentifier
            {
                Organization = "base2art",
                Product = "Echo",
                Version = new Version(1, 0, 0, 0)
            };
            var dependencyIdentifierParms = new DependencyIdentifier
            {
                Organization = "base2art",
                Product = "Echo-Parameters",
                Version = new Version(1, 0, 0, 0)
            };
            var logger = factory.CreateLoggerFor(this.TargetExectionParams(), dependencyIdentifier, dependencyIdentifierParms);
            logger.Write("Output Write");
            logger.WriteDebug("Debug Write");
            logger.WriteError("Error Write");
            logger.Append(new {
                              ABC = 123
                          });
            logger.AppendError(new {
                                   ABC = 456
                               });
            logger.Flush(new ProcedureExecutionResult(false, new VariableDataTree(dependencyIdentifier)));
            
            return logger;
        }

        private IProcedureLogger Status(MarkdownLoggerFactory factory, Guid? parentId)
        {
            var dependencyIdentifier = new DependencyIdentifier
            {
                Organization = "base2art",
                Product = "Status",
                Version = new Version(1, 0, 0, 0)
            };
            var dependencyIdentifierParms = new DependencyIdentifier
            {
                Organization = "base2art",
                Product = "Status-Parameters",
                Version = new Version(1, 0, 0, 0)
            };
            
            var logger = parentId.HasValue
                ? factory.CreateLoggerFor(this.TargetExectionParams(), parentId.Value, dependencyIdentifier, dependencyIdentifierParms)
                : factory.CreateLoggerFor(this.TargetExectionParams(), dependencyIdentifier, dependencyIdentifierParms);
            logger.Write("Output Write");
            logger.Append(new {
                              ABC = 123
                          });
            logger.AppendError(new {
                                   ABC = 456
                               });
            logger.Flush(new ProcedureExecutionResult(false, new VariableDataTree(dependencyIdentifier)));
            
            return logger;
        }
    }
}
