﻿
namespace Base2art.Bob.Executor.LogFormatters.Memory
{
    using System;
    using System.IO;
    using System.Threading;
    using Base2art.Bob.Executor.Procedures;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class MemoryLoggerFeature
    {
        [Test]
        public void ShouldCreate()
        {
            var factory = new MemoryLoggerFactory();
            factory.CreateLoggerFor(this.TargetExectionParams(), new DependencyIdentifier(), new DependencyIdentifier())
                .Should().BeOfType<MemoryLogger>()
                .And.Subject.As<MemoryLogger>().Id.Should().NotBe(Guid.Empty);
            
            Action mut = () => factory.CreateLoggerFor(this.TargetExectionParams(), Guid.Empty, new DependencyIdentifier(), new DependencyIdentifier())
                .Should().BeOfType<MemoryLogger>()
                .And.Subject.As<MemoryLogger>().Id.Should().NotBe(Guid.Empty);
            
            mut.ShouldThrow<InvalidOperationException>();
        }
        
        
        [Test]
        public void ShouldWriteToConsole()
        {
            var factory = new MemoryLoggerFactory();
            var item = new DependencyIdentifier();
            var itemPrams = new DependencyIdentifier();
            
            var targetExecutionParameters = this.TargetExectionParams();
            IReadableProcedureLogger logger = (IReadableProcedureLogger)factory.CreateLoggerFor(targetExecutionParameters, item, itemPrams);
            
            logger.Write("Output Write");
            logger.WriteDebug("Debug Write");
            logger.WriteError("Error Write");
            
            logger.Append(new {ABC = 123});
            logger.AppendError(new {ABC = 456});
            Thread.Sleep(3);
            logger.Flush(new ProcedureExecutionResult(false, new VariableDataTree(new DependencyIdentifier())));
            
            logger.OutputLogs.Length.Should().Be(2);
            logger.ErrorLogs.Length.Should().Be(2);
            logger.DebugLogs.Length.Should().Be(1);
            
            logger.OutputLogs[0].Value.As<string>().Should().Be("Output Write");
            int abc = logger.OutputLogs[1].Value.As<dynamic>().ABC;
            abc.Should().Be(123);
            
            logger.Procedure.Should().BeSameAs(item);
            logger.ProcedureParameters.Should().BeSameAs(itemPrams);
            
            logger.EndedTime.Value.Should().BeAfter(logger.StartedTime.Value);
            
            logger.LifecyclePosition.CleanLifecyclePhase
                .Should().Be(targetExecutionParameters.CurrentLifecyclePosition.CleanLifecyclePhase);
            
            logger.LifecyclePosition.DefaultLifecyclePhase
                .Should().Be(targetExecutionParameters.CurrentLifecyclePosition.DefaultLifecyclePhase);
            
            logger.LifecyclePosition.SiteLifecyclePhase
                .Should().Be(targetExecutionParameters.CurrentLifecyclePosition.SiteLifecyclePhase);
            
//            twError.GetStringBuilder().ToString().Should().Be("Error Write\r\n{\r\n  'ABC': 456\r\n}\r\n".Replace("'", "\""));
//            twOut.GetStringBuilder().ToString().Should().Be("Output Write\r\n{\r\n  'ABC': 123\r\n}\r\n".Replace("'", "\""));
//            twDebug.GetStringBuilder().ToString().Should().Be("Debug Write\r\n");
        }
    }
}
