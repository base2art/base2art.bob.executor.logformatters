﻿
namespace Base2art.Bob.Executor.LogFormatters.Console
{
    using System;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;
    using FluentAssertions;
    using NUnit.Framework;
    [TestFixture]
    public class MemoryLoggerFeature
    {
        [Test]
        public void ShouldCreate()
        {
            var factory = new ConsoleLoggerFactory();
            factory.CreateLoggerFor(this.TargetExectionParams(), new DependencyIdentifier(), new DependencyIdentifier())
                .Should().BeOfType<ConsoleLogger>()
                .And.Subject.As<ConsoleLogger>().Id.Should().NotBe(Guid.Empty);
            
            factory.CreateLoggerFor(this.TargetExectionParams(), Guid.Empty, new DependencyIdentifier(), new DependencyIdentifier())
                .Should().BeOfType<ConsoleLogger>()
                .And.Subject.As<ConsoleLogger>().Id.Should().NotBe(Guid.Empty);
        }
        
        
        [Test]
        public void ShouldWriteToConsole()
        {
            using (var twOut = new StringWriter())
            {
                
                using (var twDebug = new StringWriter())
                {
                    
                    using (var twError = new StringWriter())
                    {
                        var factory = new ConsoleLoggerFactory(
                            twOut, twDebug, twError,
                            ConsoleColor.White, ConsoleColor.Blue, ConsoleColor.DarkYellow,
                            ConsoleColor.Black, ConsoleColor.Black, ConsoleColor.Black);
                        
                        var logger = factory.CreateLoggerFor(this.TargetExectionParams(), new DependencyIdentifier(), new DependencyIdentifier());
                        
                        logger.Write("Output Write");
                        logger.WriteDebug("Debug Write");
                        logger.WriteError("Error Write");
                        
                        logger.Append(new {ABC = 123});
                        logger.AppendError(new {ABC = 456});
                        logger.Flush(new ProcedureExecutionResult(false, new VariableDataTree(new DependencyIdentifier())));
                        
                        twError.GetStringBuilder().ToString().Should().Be("Error Write\r\n{\r\n  'ABC': 456\r\n}\r\n".Replace("'", "\""));
                        twOut.GetStringBuilder().ToString().Should().Be("Output Write\r\n{\r\n  'ABC': 123\r\n}\r\n".Replace("'", "\""));
                        twDebug.GetStringBuilder().ToString().Should().Be("Debug Write\r\n");
                    }
                }
            }
        }
    }
}
