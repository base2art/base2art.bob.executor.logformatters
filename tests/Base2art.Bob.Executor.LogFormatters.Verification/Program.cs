﻿
namespace Base2art.Bob.Executor.LogFormatters.Verification
{
    using System;
    using Base2art.Bob.Executor.LogFormatters.Console;
    using Base2art.Bob.Executor.Procedures;

    class Program
    {
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("This Text Should Be Magenta");
            
            var factory = new ConsoleLoggerFactory();
            var logger = factory.CreateLoggerFor(factory.TargetExectionParams(), new DependencyIdentifier(), new DependencyIdentifier());
            
            logger.Write("Output Write");
            logger.WriteDebug("Debug Write");
            logger.WriteError("Error Write");
            
            
            logger.Append("Debug Write");
            logger.AppendError("Error Write");
            
            logger.Append(new { Message = "Debug Write" });
            logger.AppendError(new { Message = "Error Write" });
            
            logger.Flush(new ProcedureExecutionResult(false, new VariableDataTree(new DependencyIdentifier())));
            
            Console.WriteLine("This Text Should Be Magenta");
            Console.ReadKey();
        }
    }
}