﻿namespace Base2art.Bob.Executor.LogFormatters
{
    using System;
    using Base2art.Bob.Executor.Procedures;
    
    public interface IReadableProcedureLogger : Procedures.IProcedureLogger
    {
        DateTimeOffset? StartedTime { get; }
        
        DateTimeOffset? EndedTime { get; }
        
        IDependencyIdentifier Procedure { get; }
        
        IDependencyIdentifier ProcedureParameters { get; } 
        
        LifecyclePosition LifecyclePosition { get; } 
        
        IProcedureExecutionLogEntry[] ErrorLogs { get; }

        IProcedureExecutionLogEntry[] OutputLogs { get; }

        IProcedureExecutionLogEntry[] DebugLogs { get; }
    }
}
