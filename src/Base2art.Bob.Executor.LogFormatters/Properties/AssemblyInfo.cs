﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("Base2art.Bob.Executor.LogFormatters")]
[assembly: AssemblyDescription("This code is the extensible API for implementing new Loggers.")]
[assembly: AssemblyConfiguration("")]

[assembly: InternalsVisibleTo("Base2art.Bob.Executor.LogFormatters.Features, PublicKey=" +
                              "00240000048000009400000006020000002400005253413100040000010001007d8150dd1c578f" +
                              "f3d261c955a6e989fac811e3b2a573161d727bed95def1643734fe9ae59b62a1a74c982a97a2ec" +
                              "ee75d6dd9dd6596b69a3a6cc012831500d05b4c86a90b714f84e1511a0dc951fc914392f21fa49" +
                              "c31189022ef15ff00734f33e8794bf514bebd0d42ec8f72e8a8ed13a4ce38a260b62264d914f4a" +
                              "76bb8099")]