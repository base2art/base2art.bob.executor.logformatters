﻿namespace Base2art.Bob.Executor.LogFormatters
{
    using System;
    using Base2art.Bob.Executor.Procedures;

    public interface IReadableProcedureLoggerFactory : IProcedureLoggerFactory
    {
        IReadableProcedureLogger[] GetAllCreatedLoggers(Guid targetExecutionId);
    }
}
