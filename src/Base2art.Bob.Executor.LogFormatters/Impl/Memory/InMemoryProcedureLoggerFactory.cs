﻿namespace Base2art.Bob.Executor.LogFormatters.Memory
{
    using System;
    using System.Collections.Generic;
    using Base2art.Bob.Executor.Procedures;

    public class MemoryLoggerFactory : IReadableProcedureLoggerFactory
    {
        private readonly Dictionary<Guid, List<MemoryLogger>> items
            = new Dictionary<Guid, List<MemoryLogger>>();

        public IProcedureLogger CreateLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters)
        {
            Guid targetExecutionId = targetExecutionParameters.Id;
            if (!this.items.ContainsKey(targetExecutionId))
            {
                this.items.Add(targetExecutionId, new List<MemoryLogger>());
            }
            
            var logger = new MemoryLogger(procedure, procedureParameters, targetExecutionParameters.CurrentLifecyclePosition.ToLifecyclePosition());
            this.items[targetExecutionId].Add(logger);
            return logger;
        }
        
        public IProcedureLogger CreateLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            Guid parentLoggerId,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters)
        {
            var loggers = this.items[targetExecutionParameters.Id];
            var logger = this.FindParentLogger(loggers, parentLoggerId);
            if (logger == null)
            {
                throw new InvalidOperationException("Why Create a Child Logger when the parent doesn't exist?!");
            }
            
            var childLogger = new MemoryLogger(procedure, procedureParameters, targetExecutionParameters.CurrentLifecyclePosition.ToLifecyclePosition());
            logger.Children.Add(childLogger);
            return childLogger;
        }

        public IReadableProcedureLogger[] GetAllCreatedLoggers(Guid targetExecutionId)
        {
            if (this.items.ContainsKey(targetExecutionId))
            {
                return this.items[targetExecutionId].ToArray();
            }
            
            return new IReadableProcedureLogger[0];
        }
        
        private MemoryLogger FindParentLogger(IList<MemoryLogger> lookups, Guid parentLoggerId)
        {
            foreach (var item in lookups)
            {
                if (item.Id == parentLoggerId)
                {
                    return item;
                }
                
                var itemChild = this.FindParentLogger(item.Children, parentLoggerId);
                if (itemChild != null)
                {
                    return itemChild;
                }
            }
            
            return null;
        }
    }
}
