﻿namespace Base2art.Bob.Executor.LogFormatters.Memory
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Base2art.Bob.Executor.Procedures;

    public class MemoryLogger : Component, IReadableProcedureLogger
    {
        private readonly Guid id;

        private readonly List<MemoryLogger> children = new List<MemoryLogger>();
        private readonly List<ProcedureExecutionLogEntry> debug = new List<ProcedureExecutionLogEntry>();
        private readonly List<ProcedureExecutionLogEntry> error = new List<ProcedureExecutionLogEntry>();
        private readonly List<ProcedureExecutionLogEntry> output = new List<ProcedureExecutionLogEntry>();

        private readonly DependencyIdentifier procedure;

        private readonly DependencyIdentifier procedureParameters;

        private readonly LifecyclePosition lifecyclePosition;
        
        private readonly DateTimeOffset? startedTime;
        
        private DateTimeOffset? endedTime;
        
        public MemoryLogger(DependencyIdentifier procedure, DependencyIdentifier procedureParameters, LifecyclePosition lifecyclePosition)
        {
            this.lifecyclePosition = lifecyclePosition;
            this.procedureParameters = procedureParameters;
            this.procedure = procedure;
            this.id = Guid.NewGuid();
            this.startedTime = DateTimeOffset.UtcNow;
        }
        
        public Guid Id
        {
            get { return this.id; }
        }

        public IList<MemoryLogger> Children
        {
            get { return this.children; }
        }
        
        public DateTimeOffset? StartedTime
        {
            get { return this.startedTime; }
        }
        
        public DateTimeOffset? EndedTime
        {
            get { return this.endedTime; }
        }

        public IDependencyIdentifier Procedure
        {
            get { return this.procedure; }
        }

        public IDependencyIdentifier ProcedureParameters
        {
            get { return this.procedureParameters; }
        }

        public LifecyclePosition LifecyclePosition
        {
            get { return this.lifecyclePosition; }
        }

        public IProcedureExecutionLogEntry[] ErrorLogs
        {
            get { return this.error.ToArray(); }
        }

        public IProcedureExecutionLogEntry[] OutputLogs
        {
            get { return this.output.ToArray(); }
        }

        public IProcedureExecutionLogEntry[] DebugLogs
        {
            get { return this.debug.ToArray(); }
        }
        
        public void WriteDebug(string content)
        {
            this.debug.Add(Create(content));
        }

        public void Write(string content)
        {
            this.output.Add(Create(content));
        }

        public void WriteError(string content)
        {
            this.error.Add(Create(content));
        }

        public void Append<T>(T dataObject)
        {
            this.output.Add(Create(dataObject));
        }

        public void AppendError<T>(T dataObject)
        {
            this.error.Add(Create(dataObject));
        }

        public void Flush(ProcedureExecutionResult result)
        {
            if (!this.endedTime.HasValue)
            {
                this.endedTime = DateTimeOffset.UtcNow;
            }
        }
        
        private static ProcedureExecutionLogEntry Create(object content)
        {
            return new ProcedureExecutionLogEntry
            {
                Timestamp = DateTimeOffset.Now,
                Id = Guid.NewGuid(),
                Value = content
            };
        }
    }
}
