﻿namespace Base2art.Bob.Executor.LogFormatters.Console
{
    using System;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;
    
    public class ConsoleLoggerFactory : IProcedureLoggerFactory
    {
        private readonly TextWriter output;

        private readonly TextWriter debug;

        private readonly TextWriter error;

        private readonly ConsoleColor outputForegroundColor;

        private readonly ConsoleColor debugForegroundColor;

        private readonly ConsoleColor errorForegroundColor;

        private readonly ConsoleColor outputBackgroundColor;

        private readonly ConsoleColor debugBackgroundColor;

        private readonly ConsoleColor errorBackgroundColor;

        public ConsoleLoggerFactory()
            : this(
                null,
                null,
                null,
                ConsoleColor.White,
                ConsoleColor.Yellow,
                ConsoleColor.Red,
                Console.BackgroundColor,
                Console.BackgroundColor,
                Console.BackgroundColor)
        {
        }
        
        public ConsoleLoggerFactory(
            TextWriter output,
            TextWriter debug,
            TextWriter error,
            ConsoleColor outputForegroundColor,
            ConsoleColor debugForegroundColor,
            ConsoleColor errorForegroundColor,
            ConsoleColor outputBackgroundColor,
            ConsoleColor debugBackgroundColor,
            ConsoleColor errorBackgroundColor)
        {
            this.errorBackgroundColor = errorBackgroundColor;
            this.debugBackgroundColor = debugBackgroundColor;
            this.outputBackgroundColor = outputBackgroundColor;
            this.errorForegroundColor = errorForegroundColor;
            this.debugForegroundColor = debugForegroundColor;
            this.outputForegroundColor = outputForegroundColor;
            this.error = error;
            this.debug = debug;
            this.output = output;
        }
        
        public IProcedureLogger CreateLoggerFor(TargetExecutionParameters targetExecutionParameters, DependencyIdentifier procedure, DependencyIdentifier procedureParameters)
        {
            return new ConsoleLogger(
                this.output,
                this.debug,
                this.error,
                this.outputForegroundColor,
                this.debugForegroundColor,
                this.errorForegroundColor,
                this.outputBackgroundColor,
                this.debugBackgroundColor,
                this.errorBackgroundColor);
        }
        
        public IProcedureLogger CreateLoggerFor(TargetExecutionParameters targetExecutionParameters, System.Guid parentLoggerId, DependencyIdentifier procedure, DependencyIdentifier procedureParameters)
        {
            return new ConsoleLogger(
                this.output,
                this.debug,
                this.error,
                this.outputForegroundColor,
                this.debugForegroundColor,
                this.errorForegroundColor,
                this.outputBackgroundColor,
                this.debugBackgroundColor,
                this.errorBackgroundColor);
        }
    }
}
