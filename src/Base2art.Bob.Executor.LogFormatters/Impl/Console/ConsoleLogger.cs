﻿namespace Base2art.Bob.Executor.LogFormatters.Console
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using Base2art.Bob.Executor.Procedures;
    
    public class ConsoleLogger : IProcedureLogger
    {
        private readonly TextWriter output;

        private readonly TextWriter debug;

        private readonly TextWriter error;

        private readonly ConsoleColor outputForegroundColor;

        private readonly ConsoleColor debugForegroundColor;

        private readonly ConsoleColor errorForegroundColor;

        private readonly ConsoleColor outputBackgroundColor;

        private readonly ConsoleColor debugBackgroundColor;

        private readonly ConsoleColor errorBackgroundColor;

        private readonly Guid id;
        
        public ConsoleLogger(
            TextWriter output,
            TextWriter debug,
            TextWriter error,
            ConsoleColor outputForegroundColor,
            ConsoleColor debugForegroundColor,
            ConsoleColor errorForegroundColor,
            ConsoleColor outputBackgroundColor,
            ConsoleColor debugBackgroundColor,
            ConsoleColor errorBackgroundColor)
        {
            this.errorBackgroundColor = errorBackgroundColor;
            this.debugBackgroundColor = debugBackgroundColor;
            this.outputBackgroundColor = outputBackgroundColor;
            this.errorForegroundColor = errorForegroundColor;
            this.debugForegroundColor = debugForegroundColor;
            this.outputForegroundColor = outputForegroundColor;
            this.error = error;
            this.debug = debug;
            this.output = output;
            
            this.id = Guid.NewGuid();
        }
        
        public System.Guid Id
        {
            get { return this.id; }
        }
        
        public void WriteDebug(string content)
        {
            using (var holderForTextWriter = this.Debug())
            {
                holderForTextWriter.Writer.WriteLine(content);
            }
        }
        
        public void Write(string content)
        {
            using (var holderForTextWriter = this.Out())
            {
                holderForTextWriter.Writer.WriteLine(content);
            }
        }
        
        public void WriteError(string content)
        {
            using (var holderForTextWriter = this.Error())
            {
                holderForTextWriter.Writer.WriteLine(content);
            }
        }
        
        public void Append<T>(T dataObject)
        {
            this.Write(Serialize(dataObject));
        }
        
        public void AppendError<T>(T dataObject)
        {
            this.WriteError(Serialize(dataObject));
        }
        
        public void Flush(ProcedureExecutionResult result)
        {
            using (var holderForTextWriter = this.Error())
            {
                holderForTextWriter.Writer.Flush();
            }
            
            using (var holderForTextWriter = this.Debug())
            {
                holderForTextWriter.Writer.Flush();
            }
            
            using (var holderForTextWriter = this.Out())
            {
                holderForTextWriter.Writer.Flush();
            }
        }

        private static string Serialize<T>(T dataObject)
        {
            return Serializer.Serialize(dataObject);
        }
        
        private HolderForTextWriter Debug()
        {
            return new HolderForTextWriter(this.debug ?? Console.Out, this.debugBackgroundColor, this.debugForegroundColor);
        }

        private HolderForTextWriter Out()
        {
            return new HolderForTextWriter(this.output ?? Console.Out, this.outputBackgroundColor, this.outputForegroundColor);
        }

        private HolderForTextWriter Error()
        {
            return new HolderForTextWriter(this.error ?? Console.Error, this.errorBackgroundColor, this.errorForegroundColor);
        }
        
        private class HolderForTextWriter : Component
        {
            private readonly TextWriter writer;

            private readonly ConsoleColor originalForegroundColor;

            private readonly ConsoleColor originalBackgroundColor;

            public HolderForTextWriter(TextWriter writer, ConsoleColor newBackgroundColor, ConsoleColor newForegroundColor)
            {
                this.originalForegroundColor = Console.ForegroundColor;
                this.originalBackgroundColor = Console.BackgroundColor;
                
                Console.ForegroundColor = newForegroundColor;
                Console.BackgroundColor = newBackgroundColor;
                
                this.writer = writer;
            }

            public TextWriter Writer
            {
                get { return this.writer; }
            }
            
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                if (disposing)
                {
                    Console.ForegroundColor = this.originalForegroundColor;
                    Console.BackgroundColor = this.originalBackgroundColor;
                }
            }
        }
    }
}
