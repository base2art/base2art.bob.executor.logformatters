﻿namespace Base2art.Bob.Executor.LogFormatters.Markdown
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Base2art.Bob.Executor.Procedures;

    public class MarkdownLoggerFactory : IProcedureLoggerFactory
    {
        public IProcedureLogger CreateLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters)
        {
            var id = Guid.NewGuid();
            var directoryPath = targetExecutionParameters.DefaultArtifactDirectory;
            Directory.CreateDirectory(directoryPath);
            var path = Path.Combine(directoryPath, id.ToString("N") + ".md");
            var logger = new MarkdownLogger(path, id, procedure);
            
            logger.LoggerEnded += (s, e) => this.HandleCompleted(targetExecutionParameters, procedure, procedureParameters, logger.Id, null, logger.StartedTime, logger.EndedTime);
            
            return logger;
        }
        
        public IProcedureLogger CreateLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            Guid parentLoggerId,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters)
        {
            var id = Guid.NewGuid();
            var directoryPath = targetExecutionParameters.DefaultArtifactDirectory;
            Directory.CreateDirectory(directoryPath);
            var path = Path.Combine(directoryPath, id.ToString("N") + ".md");
            var logger = new MarkdownLogger(path, id, procedure);
            
            logger.LoggerEnded += (s, e) => this.HandleCompleted(
                targetExecutionParameters,
                procedure,
                procedureParameters,
                logger.Id,
                parentLoggerId,
                logger.StartedTime,
                logger.EndedTime);
            
            return logger;
        }
        
        private void HandleCompleted(
            TargetExecutionParameters targetExecutionParameters,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters,
            Guid loggerId,
            Guid? parentLoggerId,
            DateTimeOffset? startTime,
            DateTimeOffset? endedTime)
        {
            var toc = Path.Combine(targetExecutionParameters.DefaultArtifactDirectory, "_toc.json");
            if (!File.Exists(toc))
            {
                File.WriteAllText(toc, "[]");
            }
            
            var items = Serializer.Deserialize<MarkdownMetadataTree[]>(File.ReadAllText(toc)).ToList();
            
            if (!parentLoggerId.HasValue)
            {
                items.Add(new MarkdownMetadataTree
                          {
                              LoggerId = loggerId,
                              ParentLoggerId = parentLoggerId,
                              ProcedureParameters = procedureParameters,
                              Procedure = procedure,
                              LifecyclePosition = targetExecutionParameters.CurrentLifecyclePosition.ToLifecyclePosition(),
                              StartTime = startTime,
                              EndTime = endedTime
                          });
            }
            else
            {
                var parent = this.GetParentLogger(items.ToArray(), parentLoggerId.Value);
                
                var list = parent.Children.ToList();
                list.Add(new MarkdownMetadataTree
                         {
                             LoggerId = loggerId,
                             ParentLoggerId = parentLoggerId,
                             ProcedureParameters = procedureParameters,
                             Procedure = procedure,
                             LifecyclePosition = targetExecutionParameters.CurrentLifecyclePosition.ToLifecyclePosition(),
                             StartTime = startTime,
                             EndTime = endedTime
                         });
                parent.Children = list.ToArray();
            }
            
            File.WriteAllText(toc, Serializer.Serialize(items));
            
            var tocMd = Path.Combine(targetExecutionParameters.DefaultArtifactDirectory, "_toc.md");
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("# ToC");
            this.WriteTableOfContents(sb, items, 2);
            
            File.WriteAllText(tocMd, sb.ToString());
        }

        private MarkdownMetadataTree GetParentLogger(MarkdownMetadataTree[] items, Guid parentLoggerId)
        {
            foreach (var item in items)
            {
                if (item.LoggerId == parentLoggerId)
                {
                    return item;
                }
                
                var itemChild = this.GetParentLogger(item.Children, parentLoggerId);
                if (itemChild != null)
                {
                    return itemChild;
                }
            }
            
            return null;
        }

        private void WriteTableOfContents(StringBuilder sb, IEnumerable<MarkdownMetadataTree> items, int indent)
        {
            foreach (var item in items)
            {
                sb.Append('#', indent);
                sb.Append(' ');
                sb.Append('[');
                sb.AppendFormat("{0}.{1}, {2}", item.Procedure.Organization, item.Procedure.Product, item.Procedure.Version);
                sb.Append(']');
                sb.Append('(');
                sb.Append(item.LoggerId.ToString("N"));
                sb.Append(".md");
                sb.Append(')');
                sb.AppendLine();
                
                sb.Append(new string(' ', indent + 4))
                    .AppendFormat("Duration: {0}", item.EndTime.GetValueOrDefault() - item.StartTime.GetValueOrDefault())
                    .AppendLine();
                
                this.WriteTableOfContents(sb, item.Children, indent + 1);
            }
        }
    }
}
