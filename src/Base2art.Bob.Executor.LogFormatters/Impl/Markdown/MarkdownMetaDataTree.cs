﻿namespace Base2art.Bob.Executor.LogFormatters.Markdown
{
    using System;
    using Base2art.Bob.Executor.Procedures;

    internal class MarkdownMetadataTree
    {
        private MarkdownMetadataTree[] children;

        public LifecyclePosition LifecyclePosition { get; set; }

        public DependencyIdentifier Procedure { get; set; }
        
        public DependencyIdentifier ProcedureParameters { get; set; }

        public Guid LoggerId { get; set; }

        public Guid? ParentLoggerId { get; set; }
        
        public DateTimeOffset? StartTime { get; set; }
        
        public DateTimeOffset? EndTime { get; set; }
        
        public MarkdownMetadataTree[] Children
        {
            get { return this.children = this.children ?? new MarkdownMetadataTree[0]; }
            set { this.children = value; }
        }
    }
}
