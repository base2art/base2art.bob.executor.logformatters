﻿namespace Base2art.Bob.Executor.LogFormatters.Markdown
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Base2art.Bob.Executor.Procedures;

    public class MarkdownLogger : IProcedureLogger
    {
        private readonly List<ProcedureExecutionLogEntry> debugs = new List<ProcedureExecutionLogEntry>();
        private readonly List<ProcedureExecutionLogEntry> errors = new List<ProcedureExecutionLogEntry>();
        private readonly List<ProcedureExecutionLogEntry> outputs = new List<ProcedureExecutionLogEntry>();
        
        private readonly Guid id;
        
        private readonly DependencyIdentifier procedure;
        
        private readonly DateTimeOffset? startedTime;

        private readonly string file;
        
        private DateTimeOffset? endedTime;
        
        public MarkdownLogger(
            string file,
            Guid id,
            DependencyIdentifier procedure)
        {
            this.file = file;
            this.id = id;
            this.procedure = procedure;
            this.startedTime = DateTimeOffset.UtcNow;
        }
        
        public event EventHandler<EventArgs> LoggerEnded;
        
        public Guid Id
        {
            get { return this.id; }
        }
        
        public DateTimeOffset? StartedTime
        {
            get { return this.startedTime; }
        }
        
        public DateTimeOffset? EndedTime
        {
            get { return this.endedTime; }
        }

        public IDependencyIdentifier Procedure
        {
            get { return this.procedure; }
        }
        
        public void WriteDebug(string content)
        {
            this.debugs.Add(Create(content));
        }

        public void Write(string content)
        {
            this.outputs.Add(Create(content));
        }

        public void WriteError(string content)
        {
            this.errors.Add(Create(content));
        }

        public void Append<T>(T dataObject)
        {
            this.outputs.Add(Create(dataObject));
        }

        public void AppendError<T>(T dataObject)
        {
            this.errors.Add(Create(dataObject));
        }

        public void Flush(ProcedureExecutionResult result)
        {
            if (!this.endedTime.HasValue)
            {
                this.endedTime = DateTimeOffset.UtcNow;
            }
            
            StringBuilder sb = new StringBuilder();
            
            var status = "Inconclusive";
            if (result.IsSuccessfulRun.HasValue)
            {
                status = result.IsSuccessfulRun.Value ? "Success" : "Failed";
            }
            
            sb.AppendFormat("# {0} [{1}]", this.Procedure.Product, status);
            sb.AppendLine();
            sb.AppendFormat("## {0} {1}", this.Procedure.Organization, this.Procedure.Version);
            sb.AppendLine();
            sb.AppendLine();
            AppendMessages(sb, "Errors", this.errors);
            AppendMessages(sb, "Output", this.outputs);
            AppendMessages(sb, "Debugs", this.debugs);
            
            File.WriteAllText(this.file, sb.ToString());
            
            this.OnLoggerEnded(EventArgs.Empty);
        }

        protected virtual void OnLoggerEnded(EventArgs e)
        {
            var handler = this.LoggerEnded;
            
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private static string Serialize(object value)
        {
            var str = value as string;
            if (str != null)
            {
                return str;
            }
            
            var content = Serializer.Serialize(value);
            string[] lines = content.Replace("\r\n", "\n").Split(new[] { '\n' });
            return
                string.Concat(
                    Environment.NewLine,
                    string.Join(Environment.NewLine, lines.Select(s => new string(' ', 4) + s).ToArray()),
                    Environment.NewLine);
        }
        
        private static ProcedureExecutionLogEntry Create(object content)
        {
            return new ProcedureExecutionLogEntry
            {
                Timestamp = DateTimeOffset.Now,
                Id = Guid.NewGuid(),
                Value = content
            };
        }
        
        private static void AppendMessage(StringBuilder sb, ProcedureExecutionLogEntry msg)
        {
            sb.AppendFormat("#### [{0}]", msg.Timestamp);
            sb.AppendLine();
            sb.AppendLine(Serialize(msg.Value));
        }

        private static void AppendMessages(StringBuilder sb, string sectionType,  List<ProcedureExecutionLogEntry> messages)
        {
            if (messages.Count > 0)
            {
                sb.AppendLine("### " + sectionType);
                foreach (var msg in messages)
                {
                    AppendMessage(sb, msg);
                }
            }
        }
    }
}
