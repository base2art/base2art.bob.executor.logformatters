﻿namespace Base2art.Bob.Executor.LogFormatters
{
    using System;
    
    public interface IProcedureExecutionLogEntry
    {
        Guid Id { get; }

        DateTimeOffset Timestamp { get; }

        object Value { get; }
    }
}
