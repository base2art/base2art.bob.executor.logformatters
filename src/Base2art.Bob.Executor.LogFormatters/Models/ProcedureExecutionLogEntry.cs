﻿namespace Base2art.Bob.Executor.LogFormatters
{
    using System;

    internal class ProcedureExecutionLogEntry : IProcedureExecutionLogEntry
    {
        public Guid Id { get; set; }

        public DateTimeOffset Timestamp { get; set; }

        public object Value { get; set; }
    }
}
